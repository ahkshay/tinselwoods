class MovieController < ApplicationController

  def new_movie_seed_request
    @languages = Language.all.by_lang_name
  end

  def seed_request_for_movie
    @movie_title = params["mov_title"]
    @movie_language = params["mov_language"]
    if params.present? && params["mov_title"].present? && params["mov_language"].present?
      Delayed::Job.enqueue SeedMovieViaTmdbJob.new(params["mov_title"], params["mov_language"])
      flash.now[:success] = 'Request for seeding has been submitted successfully'
    else
      flash[:danger] = 'Please enter valid movie title and movie language in order to proceed with the seeding request'
      redirect_to action: "new_movie_seed_request"
    end
  end

end
