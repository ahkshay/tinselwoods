class ProductionHouseController < ApplicationController

  def index
    @prod_houses = ProductionHouse.all.limit(15)
  end

  def show
    @prod_house = ProductionHouse.find_by_id(params[:id])
  end

  def edit
    @prod_house = ProductionHouse.find_by_id(params[:id])
  end

  def update

    @prod_house = ProductionHouse.find_by_id(params[:id])

    if @prod_house.update_attributes(prod_house_params_update)
      flash.now[:success] = 'Update Request Was Successful.'
      render "show"
    else
      flash.now[:warning] = 'Update Request Has Failed.Try Again.'
      render "edit"
    end

  end

  private

  def prod_house_params_update
    params.require(:production_house).permit(:ph_name, :ph_headquarters, :ph_establishment_year, :ph_description)
  end

end
