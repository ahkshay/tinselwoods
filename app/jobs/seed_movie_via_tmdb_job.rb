require 'themoviedb-api'

class SeedMovieViaTmdbJob < ApplicationJob

  queue_as :movie_seeding_queue

  def perform(*args)
    SeedMovieLog.info("Inside Perform Method")
    if args[0].present?
      mov_title = args[0]
      SeedMovieLog.debug("Title is #{mov_title}")
      mov_language = args[1]
      SeedMovieLog.debug("Language is #{mov_language}")
      movie = search_for_title(mov_title)
      SeedMovieLog.debug("Movie is #{movie}")
      if movie.results.present?
        movie_match_results = movie.results
        movie_match_index = fetch_best_match_for_title(movie_match_results, mov_language, mov_title) if !movie.results.empty? && movie.results.is_a?(Array)
        SeedMovieLog.debug("movie_match_index is #{movie_match_index}")
        movie_id = movie_match_results[movie_match_index].id
        SeedMovieLog.debug("TMDb Movie ID is #{movie_id}")
        fetch_movie_details(movie_id, mov_title, mov_language)
      else
        #Log the seed attempt as an failure in the App Diagnostics Log Table.
        SeedStatusLog.create(:seed_parameters => mov_title, :seed_request_status => false, :seed_entity_type => 'No Movie Data')
        SeedMovieLog.error("No Movie Data Found For #{mov_title}")
        #Gracefully Handle No Results for Movie Error.
        raise MovieSeedJobErrors::NoResultsMovieError.new("No results matching movie #{mov_title} and language #{mov_language}")
      end
    end
  end

  def fetch_movie_details(movie_id, mov_title, mov_lang)
    prod_house_data_arr = []
    movie_cast_data_arr = []
    movie_crew_data_arr = []
    movie_backdrop_data_arr = []
    movie_data = seed_movie_data(movie_id)
    SeedMovieLog.debug("movie_data is #{movie_data}")
    if movie_data.production_companies.present?
      prod_houses = movie_data.production_companies
      SeedMovieLog.debug("prod_houses is #{prod_houses}")
      prod_houses.each do |prod_house|
        ph_data = seed_production_house_data(prod_house.id)
        prod_house_data_arr << ph_data
      end
      SeedMovieLog.debug("prod house data arr is #{prod_house_data_arr}")
      prod_house_indices = ProductionHouse.create_prod_house_from_response(prod_house_data_arr)
    else
      #Log the attempt as an failure in the Attempts Table.
      SeedStatusLog.create(:seed_parameters => mov_title, :seed_request_status => false, :seed_entity_type => 'No Production House')
    end
    movie_record = save_movie_record(movie_id, mov_lang, movie_data, prod_house_indices) if (movie_data.present? && prod_house_indices.is_a?(Array) && !prod_house_indices.empty?)
    movie_casts = seed_movie_cast_data(movie_id)
    movie_crews = seed_movie_crew_data(movie_id)
    movie_backdrops = seed_movie_backdrop_data(movie_id)
    p "movie_backdrops is #{movie_backdrops}"
    SeedMovieLog.debug("movie_casts is #{movie_casts}")
    SeedMovieLog.debug("movie_crews is #{movie_crews}")
    SeedMovieLog.debug("movie_backdrops is #{movie_backdrops}")
    movie_backdrop_data_arr = movie_backdrops.map {|mov_bkdp| mov_bkdp.file_path} if (movie_backdrops.is_a?(Array) && !movie_backdrops.empty?) #Returns Collection of Concerned Movie Backdrop Pictures.
    p "movie_backdrop_data_arr is #{movie_backdrop_data_arr}"
    movie_cast_index = movie_casts.map {|movie_cast| movie_cast.id} if (movie_casts.is_a?(Array) && !movie_casts.empty?) # Returns Collection of Cast Member ID's who have acted in the movie.
    movie_char_names = movie_casts.map {|movie_cast| movie_cast.character} if (movie_casts.is_a?(Array) && !movie_casts.empty?) # Returns Collection of Character Names who have acted in the movie.
    movie_cast_data_arr = seed_movie_people_data(movie_cast_index) if (movie_cast_index.is_a?(Array) && !movie_cast_index.empty?)
    movie_crew_index = movie_crews.map {|movie_crew| movie_crew.id} if (movie_crews.is_a?(Array) && !movie_crews.empty?)  # Returns Collection of Crew Member ID's who have worked in the movie.
    movie_crew_depts = movie_crews.map {|movie_crew| movie_crew.job} if (movie_crews.is_a?(Array) && !movie_crews.empty?) # Returns Collection of Departments of Crew Members who have worked in the movie.
    movie_crew_data_arr = seed_movie_people_data(movie_crew_index) if (movie_crew_index.is_a?(Array) && !movie_crew_index.empty?)
    SeedMovieLog.debug("movie_cast_data_arr is #{movie_cast_data_arr}")
    SeedMovieLog.debug("movie_crew_data_arr is #{movie_crew_data_arr}")
    SeedMovieLog.debug("movie_backdrop_data_arr is #{movie_backdrop_data_arr}")
    movie_cast_ids = Artist.create_artist_from_response(movie_cast_data_arr, movie_char_names, movie_record) unless movie_cast_data_arr.empty?
    movie_crew_ids = Crew.create_crew_from_response(movie_crew_data_arr, movie_crew_depts, movie_record) unless movie_crew_data_arr.empty?
    MovieBackdrop.create_backdrops_from_response(movie_backdrop_data_arr, movie_record) unless movie_backdrop_data_arr.empty?
  end

  def save_movie_record(movie_id, movie_lang, movie_data, prod_house_indices)
    movie_video_data = seed_movie_video_data(movie_id)
    movie = Movie.create_movie_from_response(movie_data, prod_house_indices, movie_video_data, movie_lang)
  end

  def fetch_best_match_for_title(movie_result_set, language_matcher, title)
    match_index_arr = []
    movie_result_set.each_with_index do |movie, index|
      if (movie.original_language == language_matcher && movie.title == title) #Best Match-Case for choosing correct Movie Record from Result Set, Requires Both Movie Title and Movie Language to be passed strictly in order to be chosen for consideration.
        match_index_arr << index
      else (movie.title == title) #Worst Match-Case for choosing correct Movie record from Result Set
        match_index_arr << index
      end
    end
    return match_index_arr.first
  end

  def seed_movie_people_data(movie_people_indices)
    movie_people_data_arr = []
    movie_people_indices.each do |mp_id|
      mp_data = seed_movie_person_data(mp_id)
      movie_people_data_arr << mp_data
    end
    return movie_people_data_arr
  end

  def search_for_title(title)
    Tmdb::Search.movie(title)
  end

  def seed_movie_data(movie_id)
    Tmdb::Movie.detail(movie_id)
  end

  def seed_movie_video_data(movie_id)
    Tmdb::Movie.videos(movie_id)
  end

  def seed_movie_cast_data(movie_id)
    Tmdb::Movie.cast(movie_id)
  end

  def seed_movie_crew_data(movie_id)
    Tmdb::Movie.crew(movie_id)
  end

  def seed_movie_person_data(mp_id)
    Tmdb::Person.detail(mp_id)
  end

  def seed_production_house_data(ph_id)
    Tmdb::Company.detail(ph_id)
  end

  def seed_movie_backdrop_data(movie_id)
    Tmdb::Movie.backdrops(movie_id)
  end

end
