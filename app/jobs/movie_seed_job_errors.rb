module MovieSeedJobErrors

  class MovieSeedJobError < StandardError
  end

  class NoResultsMovieError < MovieSeedJobError
  end

  class APIUnreachableError < MovieSeedJobError
  end

  class AnonymousError < MovieSeedJobError
  end

  class NoProdHouseDataError < MovieSeedJobError
  end

end
