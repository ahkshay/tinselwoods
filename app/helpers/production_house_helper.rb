module ProductionHouseHelper

  def fill_up_blank_values_ph_year(ph)

    if ph.ph_establishment_year.nil?
      return "Not Applicable"
    else
      return ph.ph_establishment_year
    end

  end

end
