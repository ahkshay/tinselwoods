module ApplicationHelper

  def bootstrap_alert_class_for(flash_type)
    {
      :success => 'alert-success',
      :danger => 'alert-danger',
      :warning => 'alert-warning',
      :info => 'alert-primary'
    } [flash_type.to_sym] || flash_type.to_s
  end

end
