# == Schema Information
#
# Table name: movie_posters
#
#  id               :bigint(8)        not null, primary key
#  movie_id         :bigint(8)
#  mov_backdrop_url :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class MovieBackdrop < ApplicationRecord

  #Model Related Constant Declarations Goes Here.....

  # ActiveRecord Association Declarations Goes Here.....
  belongs_to :movie

  # ActiveRecord Validation Declarations Goes Here.....

  #ActiveRecord Callback Declarations Goes Here.....
  before_save :set_backdrop_url, on: [:create_backdrops_from_response]

  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Signature are all Class Methods.....

  def self.create_backdrops_from_response(mov_backdrop_pl, movie)
    p "mov_backdrop_pl is #{mov_backdrop_pl}"
    mov_backdrop_pl.each do |mov_bkdp_file|
      movie.movie_backdrops.create!(:mov_backdrop_url => mov_bkdp_file)
    end
  end

  private

  def set_backdrop_url
    unless self.mov_backdrop_url.nil?
      self.mov_backdrop_url = (BACKDROP_BASE_URL + mov_backdrop_url) if mov_backdrop_url.present?
    end
  end

end
