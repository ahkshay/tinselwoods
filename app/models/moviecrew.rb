# == Schema Information
#
# Table name: moviecrews
#
#  id         :bigint(8)        not null, primary key
#  movie_id   :integer
#  crew_id    :integer
#  crew_title :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Moviecrew < ApplicationRecord
  #ActiveRecord Association Declarations Goes Here.....
  belongs_to :movie
  belongs_to :crew

  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Signature are all Class Methods.....
  def self.link_movie_and_artists(crew_indices, crew_dept_data, movie)
    crew_indices.each_with_index do |crew_index, index|
      Moviecrew.create(:movie_id => movie.id, :crew_id => crew_index, :crew_title => crew_dept_data[index])
    end
  end

end
