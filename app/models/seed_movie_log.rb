class SeedMovieLog
  LogFile = Rails.root.join('log', 'movie_seed.log')
  class << self
    cattr_accessor :logger
    delegate :debug, :info, :warn, :error, :fatal, :to => :logger
  end
end
