# == Schema Information
#
# Table name: languages
#
#  id            :bigint(8)        not null, primary key
#  lang_name     :string
#  lang_iso_code :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Language < ApplicationRecord
  #ActiveRecord Scope declarations Goes Here.....
  scope :by_lang_name, -> { order(:lang_name) }
end
