# == Schema Information
#
# Table name: production_houses
#
#  id              :bigint(8)        not null, primary key
#  ph_name         :string
#  ph_country      :string
#  ph_headquarters :string
#  ph_description  :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  ph_homepage_url :string
#  ph_logo_path    :string
#  ph_tmdb_id      :integer
#

class ProductionHouse < ApplicationRecord

  # ActiveRecord Association Declarations Goes Here.....
  has_many :director_creations
  has_many :directors, through: :director_creations
  has_many :creations
  has_many :movies, through: :creations

  # ActiveRecord Validation Declarations Goes Here.....
  validates :ph_name, presence: true, uniqueness: true

  #ActiveRecord Callback Declarations Goes Here.....
  before_save :set_logo_url, on: [:create_prod_house_from_response]

  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Signature are all Class Methods.....

  def self.create_prod_house_from_response(ph_payload)
    prod_house_indexes = []
    if ph_payload.present?
      ph_payload.each do |phpl|
        SeedMovieLog.info("Stripping out all parameters from production house data payload recieved")
        prod_house_name = phpl.name if phpl.name.present?
        prod_house_country = phpl.origin_country if phpl.origin_country.present?
        prod_house_hq = phpl.headquarters if phpl.headquarters.present?
        prod_house_desc = phpl.description if phpl.description.present?
        prod_house_homepage = phpl.homepage if phpl.homepage.present?
        prod_house_logo_path = phpl.logo_path if phpl.logo_path.present?
        prod_house_tmdb_id = phpl.id if phpl.id.present?
        if ProductionHouse.find_by_ph_tmdb_id(prod_house_tmdb_id).present?
          SeedMovieLog.info("Record Exists for Production House #{prod_house_name}")
          prod_house_indexes << ProductionHouse.find_by_ph_tmdb_id(prod_house_tmdb_id).id
        else
          SeedMovieLog.info("Attempting to save record for #{prod_house_name}")
          prod_house = ProductionHouse.create(:ph_name => prod_house_name, :ph_country => prod_house_country, :ph_headquarters => prod_house_hq, :ph_description => prod_house_desc, :ph_homepage_url => prod_house_homepage , :ph_logo_path => prod_house_logo_path, :ph_tmdb_id => prod_house_tmdb_id)
          SeedMovieLog.info("Saved record for #{prod_house_name}") if prod_house.persisted?
          prod_house_indexes << prod_house.id
        end
      end
    end
    SeedMovieLog.info("No Production House Found for Movie. Bailing out with Fallback Mechanism") if prod_house_indexes.empty?
    prod_house_indexes << fetch_fallback_prodhouse_record if prod_house_indexes.empty? # Used as a fallback mechanism record for associating between Movie and ProductionHouse records in case we fail to fetch any production house information for the movie being seeded.
    return prod_house_indexes
  end

  def fetch_fallback_prodhouse_record
    ProductionHouse.find_by(:ph_name => 'NoProdHouse').id
  end

  private

  def set_logo_url
    unless self.ph_logo_path.nil?
      self.ph_logo_path = PROD_HOUSE_BASE_URL + ph_logo_path
    end
  end

end
