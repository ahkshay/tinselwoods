# == Schema Information
#
# Table name: comments
#
#  id           :bigint(8)        not null, primary key
#  comment_body :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  review_id    :bigint(8)
#

class Comment < ApplicationRecord
  belongs_to :review
end
