# == Schema Information
#
# Table name: moviecasts
#
#  id             :bigint(8)        not null, primary key
#  artist_id      :integer
#  movie_id       :integer
#  character_name :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Moviecast < ApplicationRecord
  #ActiveRecord Association Declarations Goes Here.....
  belongs_to :movie
  belongs_to :artist

  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Signature are all Class Methods.....
  def self.link_movie_and_artists(artist_indices, artist_char_data, movie)
    artist_indices.each_with_index do |artist_index, index|
      Moviecast.create(:movie_id => movie.id, :artist_id => artist_index, :character_name => artist_char_data[index])
    end
  end

end
