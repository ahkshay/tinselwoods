class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  #All Global Model-related Constant Declarations Goes Here.....
  PROD_HOUSE_BASE_URL = 'http://image.tmdb.org/t/p/w300'
  POSTER_BASE_URL = 'http://image.tmdb.org/t/p/w300'
  BACKDROP_BASE_URL = 'http://image.tmdb.org/t/p/w780'
  YOUTUBE_BASE_URL = 'https://www.youtube.com/watch?v='
  ARTIST_BASE_URL = 'https://image.tmdb.org/t/p/w300'
  CREW_BASE_URL = 'https://image.tmdb.org/t/p/w300'
end
