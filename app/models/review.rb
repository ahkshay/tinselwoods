# == Schema Information
#
# Table name: reviews
#
#  id                    :bigint(8)        not null, primary key
#  review_title          :string
#  review_body           :text
#  review_watchable      :string
#  is_review_publishable :boolean
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class Review < ApplicationRecord
  has_many :comments
end
