# == Schema Information
#
# Table name: movies
#
#  id                    :bigint(8)        not null, primary key
#  mov_title             :string
#  mov_plot              :text
#  mov_run_time          :integer
#  mov_release_date      :date
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  mov_release_year      :integer
#  mov_country           :string
#  mov_poster_url        :string
#  mov_imdb_reference_id :string
#  mov_imdb_rating       :float
#  mov_youtube_url       :string
#  mov_genres            :text             default([]), is an Array
#  mov_backdrop_url      :string
#  mov_tmdb_id           :integer
#  mov_language          :string
#  mov_status            :string
#  mov_tagline           :string
#  mov_budget            :integer
#  mov_revenue           :integer
#

class Movie < ApplicationRecord

  #Model-Related Constant Declarations Goes Here.....
  LANGUAGE_LOOKUP_DICT = HashWithIndifferentAccess.new({:en => 'English', :hi => 'Hindi', :kn => 'Kannada', :ml => 'Malayalam', :mr => 'Marathi', :ta => 'Tamil', :te => 'Telugu'})

  #ActiveRecord Association Declarations Goes Here.....
  has_many :creations
  has_many :production_houses, through: :creations
  has_many :moviecasts
  has_many :artists, through: :moviecasts
  has_many :moviecrews
  has_many :crews, through: :moviecrews
  has_many :movie_backdrops

  #ActiveRecord Validation Declarations Goes Here.....
  validates :mov_title, presence: true

  #ActiveRecord Callback Declarations Goes Here.....
  before_save :set_poster_url, on: [:create_movie_from_response]
  before_save :set_backdrop_url, on: [:create_movie_from_response]
  before_save :set_youtube_url , on: [:create_movie_from_response]
  before_save :set_release_year, on: [:create_movie_from_response]

  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Method Signature are all Class Methods.....
  def self.create_movie_from_response(mov_payload, prod_house_indices, mov_video_payload, mov_language)
    SeedMovieLog.info("Inside Movie Model Create Function")
    if mov_payload.present?
      SeedMovieLog.info("Stripping out all parameters from movie data payload recieved")
      movie_title = mov_payload.title if mov_payload.title.present?
      movie_plot = mov_payload.overview if mov_payload.overview.present?
      movie_runtime = mov_payload.runtime if mov_payload.runtime.present?
      movie_release_date = mov_payload.release_date if mov_payload.release_date.present?
      movie_language = LANGUAGE_LOOKUP_DICT[mov_language.to_sym]
      movie_country = mov_payload.production_countries.first.name if mov_payload.production_countries.present?
      movie_poster_url = mov_payload.poster_path if mov_payload.poster_path.present?
      movie_imdb_id = mov_payload.imdb_id if mov_payload.imdb_id.present?
      movie_tmdb_id = mov_payload.id if mov_payload.id.present?
      movie_imdb_rating = mov_payload.vote_average if mov_payload.vote_average.present?
      movie_backdrop_url = mov_payload.backdrop_path if mov_payload.backdrop_path.present?
      movie_genre_arr = build_genre_arr_movie(mov_payload.genres) if (mov_payload.genres.present? && mov_payload.genres.is_a?(Array) && !mov_payload.genres.empty?)
      movie_youtube_url = extract_movie_youtube_data(mov_video_payload) if (mov_video_payload.present? && mov_video_payload.is_a?(Array) && !mov_video_payload.empty?)
      movie_status = mov_payload.status if mov_payload.status.present?
      movie_tagline = mov_payload.tagline if mov_payload.tagline.present?
      movie_revenue = mov_payload.revenue if mov_payload.revenue.present?
      movie_budget = mov_payload.budget if mov_payload.budget.present?
    end
    SeedMovieLog.info("Attempting to save record for #{movie_title}")
    movie = Movie.create(:mov_title => movie_title, :mov_plot => movie_plot, :mov_run_time => movie_runtime, :mov_release_date => movie_release_date, :mov_language => movie_language, :mov_country => movie_country, :mov_poster_url => movie_poster_url, :mov_backdrop_url => movie_backdrop_url, :mov_imdb_reference_id => movie_imdb_id, :mov_tmdb_id => movie_tmdb_id, :mov_imdb_rating => movie_imdb_rating, :mov_genres => movie_genre_arr, :mov_youtube_url => movie_youtube_url,
      :mov_status => movie_status, :mov_tagline => movie_tagline, :mov_revenue => movie_revenue, :mov_budget => movie_budget)
    SeedMovieLog.info("Inserted record successfully for #{movie_title}") if movie.persisted?
    SeedMovieLog.info("Attempting to associate movie and production houses")
    Creation.link_movie_and_prod_houses(prod_house_indices, movie) #Associate Movies and Production House Together
    return movie
  end

  def self.build_genre_arr_movie(movie_genres_arr)
    genre_arr = Array.new()
    movie_genres_arr.each do |mov_genre|
      genre_arr << mov_genre.name
    end
    return genre_arr
  end

  def self.extract_movie_youtube_data(video_data_arr)
    youtube_keys = Array.new()
    video_data_arr.each do |video_datum|
      youtube_keys << video_datum.key if (video_datum.type == 'Trailer' || video_datum.type == 'Teaser')
    end
    return youtube_keys[0]
  end

  private

  def set_poster_url
    unless self.mov_poster_url.nil?
      self.mov_poster_url = (POSTER_BASE_URL + mov_poster_url) if mov_poster_url.present?
    end
  end

  def set_backdrop_url
    unless self.mov_backdrop_url.nil?
      self.mov_backdrop_url = (BACKDROP_BASE_URL + mov_backdrop_url) if mov_backdrop_url.present?
    end
  end

  def set_youtube_url
    unless self.mov_youtube_url.nil?
      self.mov_youtube_url = (YOUTUBE_BASE_URL + mov_youtube_url) if mov_youtube_url.present?
    end
  end

  def set_release_year
    self.mov_release_year = (mov_release_date.year) if mov_release_date.present?
  end

end
