# == Schema Information
#
# Table name: creations
#
#  id                  :bigint(8)        not null, primary key
#  movie_id            :bigint(8)
#  production_house_id :bigint(8)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Creation < ApplicationRecord
  #ActiveRecord Association Declarations Goes Here.....
  belongs_to :movie
  belongs_to :production_house

  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Signature are all Class Methods.....
  def self.link_movie_and_prod_houses(prod_house_indices, movie)
    prod_house_indices.each do |prod_house_index|
      Creation.create(:movie_id => movie.id, :production_house_id => prod_house_index)
    end
  end

end
