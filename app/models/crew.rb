# == Schema Information
#
# Table name: crews
#
#  id                     :bigint(8)        not null, primary key
#  crw_dob                :date
#  crw_dod                :date
#  crw_primary_department :string
#  crw_tmdb_id            :integer
#  crw_name               :string
#  crw_aliases            :string           default([]), is an Array
#  crw_biography          :text
#  crw_popularity         :float
#  crw_birth_place        :string
#  crw_profile_photo      :string
#  crw_homepage_url       :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  crw_imdb_id            :integer
#  crw_gender             :string
#

class Crew < ApplicationRecord

  #Model-Related Constant Declarations Goes Here.....
  CREW_GENDER_LOOKUP_DICT = HashWithIndifferentAccess.new({:m => 'Male', :f => 'Female'})

  #ActiveRecord Association Declarations Goes Here.....
  has_many :moviecrews
  has_many :movies, through: :moviecrews

  # ActiveRecord Validation Declarations Goes Here.....

  #ActiveRecord Callback Declarations Goes Here.....
  before_save :set_profile_url, on: [:create_crew_from_response]


  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Signature are all Class Methods.....

  def self.create_crew_from_response(crew_data_pl, crew_dept_data_pl, movie)
    SeedMovieLog.info('Inside Crew Model Create Function')
    crew_indexes = []
    if crew_data_pl.present? && crew_data_pl.is_a?(Array)
      crew_data_pl.each do |crew_data|
        if Crew.find_by_crw_tmdb_id(crew_data.id).present?
          SeedMovieLog.info("Record Exists for Crew Member #{crew_data.name}")
          crew_indexes << Crew.find_by_crw_tmdb_id(crew_data.id).id
        else
          crw_name = crew_data.name if crew_data.name.present?
          crw_tmdb_id = crew_data.id if crew_data.id.present?
          crw_imdb_id = crew_data.imdb_id if crew_data.imdb_id.present?
          crw_dob = crew_data.birthday if crew_data.birthday.present?
          crw_dod = crew_data.deathday if crew_data.deathday.present?
          crw_primary_skill = crew_data.known_for_department if crew_data.known_for_department.present?
          crw_aliases = crew_data.also_known_as if crew_data.also_known_as.present?
          crw_gender = (crew_data.gender == 2 ? CREW_GENDER_LOOKUP_DICT['m'] : CREW_GENDER_LOOKUP_DICT['f']) if crew_data.gender.present?
          crw_biography = crew_data.biography if crew_data.biography.present?
          crw_popularity = crew_data.popularity if crew_data.popularity.present?
          crw_birth_place = crew_data.place_of_birth if crew_data.place_of_birth.present?
          crw_profile_photo = crew_data.profile_path if crew_data.profile_path.present?
          crw_homepage_url = crew_data.homepage if crew_data.homepage.present?
          SeedMovieLog.info("Saving Record for Crew Member #{crw_name}")
          crew = Crew.create(:crw_name => crw_name, :crw_tmdb_id => crw_tmdb_id, :crw_dob => crw_dob, :crw_dod => crw_dod, :crw_primary_department => crw_primary_skill, :crw_aliases => crw_aliases, :crw_gender => crw_gender, :crw_biography => crw_biography, :crw_popularity => crw_popularity, :crw_birth_place => crw_birth_place, :crw_profile_photo => crw_profile_photo, :crw_imdb_id => crw_imdb_id, :crw_homepage_url => crw_homepage_url)
          SeedMovieLog.info("Saved record for Crew Member #{crw_name}") if crew.persisted?
          crew_indexes << crew.id
        end
      end
    end
    crew_indexes
    Moviecrew.link_movie_and_artists(crew_indexes, crew_dept_data_pl, movie)
  end

  private

  def set_profile_url
    unless self.crw_profile_photo.nil?
      self.crw_profile_photo = CREW_BASE_URL + crw_profile_photo
    end
  end

end
