# == Schema Information
#
# Table name: seed_status_logs
#
#  id                  :bigint(8)        not null, primary key
#  seed_parameters     :string
#  seed_entity_type    :string
#  seed_request_status :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class SeedStatusLog < ApplicationRecord
end
