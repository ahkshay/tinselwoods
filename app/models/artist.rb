# == Schema Information
#
# Table name: artists
#
#  id                :bigint(8)        not null, primary key
#  art_name          :string
#  art_tmdb_id       :integer
#  art_dob           :date
#  art_dod           :date
#  art_primary_skill :string
#  art_aliases       :string           default([]), is an Array
#  art_gender        :string
#  art_biography     :text
#  art_popularity    :float
#  art_birth_place   :string
#  art_profile_photo :string
#  art_imdb_id       :string
#  art_homepage_url  :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Artist < ApplicationRecord

  #Model Related Constant Declarations Goes Here.....
  ARTIST_GENDER_LOOKUP_DICT = HashWithIndifferentAccess.new({:m => 'Male', :f => 'Female'})

  # ActiveRecord Association Declarations Goes Here.....
  has_many :moviecasts
  has_many :movies, through: :moviecasts

  # ActiveRecord Validation Declarations Goes Here.....
  validates :art_name, presence: true
  validates :art_tmdb_id, presence: true
  validates :art_primary_skill, presence: true

  #ActiveRecord Callback Declarations Goes Here.....
  before_save :set_profile_url, on: [:create_artist_from_response]

  # All Instance and Class Method Declaration Goes Here.....
  # Method Declarations Starting with an 'self.' in Signature are all Class Methods.....

  def self.create_artist_from_response(artists_data_pl, artist_char_data_pl, movie)
    SeedMovieLog.info('Inside Artist Model Create Function')
    artist_indexes = []
    if artists_data_pl.present? && artists_data_pl.is_a?(Array)
      artists_data_pl.each do |art_data|
        if Artist.find_by_art_tmdb_id(art_data.id).present?
          SeedMovieLog.info("Record Exists for Cast Member #{art_data.name}")
          artist_indexes << Artist.find_by_art_tmdb_id(art_data.id).id
        else
          art_name = art_data.name if art_data.name.present?
          art_tmdb_id = art_data.id if art_data.id.present?
          art_dob = art_data.birthday if art_data.birthday.present?
          art_dod = art_data.deathday if art_data.deathday.present?
          art_primary_skill = art_data.known_for_department if art_data.known_for_department.present?
          art_aliases = art_data.also_known_as if art_data.also_known_as.present?
          art_gender = (art_data.gender == 1 ? ARTIST_GENDER_LOOKUP_DICT['f'] : ARTIST_GENDER_LOOKUP_DICT['m']) if art_data.gender.present?
          art_biography = art_data.biography if art_data.biography.present?
          art_popularity = art_data.popularity if art_data.popularity.present?
          art_birth_place = art_data.place_of_birth if art_data.place_of_birth.present?
          art_profile_photo = art_data.profile_path if art_data.profile_path.present?
          art_imdb_id = art_data.imdb_id if art_data.imdb_id.present?
          art_homepage_url = art_data.homepage if art_data.homepage.present?
          SeedMovieLog.info("Saving Record for Cast Member #{art_name}")
          artist = Artist.create(:art_name => art_name, :art_tmdb_id => art_tmdb_id, :art_dob => art_dob, :art_dod => art_dod, :art_primary_skill => art_primary_skill, :art_aliases => art_aliases, :art_gender => art_gender, :art_biography => art_biography, :art_popularity => art_popularity, :art_birth_place => art_birth_place, :art_profile_photo => art_profile_photo, :art_imdb_id => art_imdb_id, :art_homepage_url => art_homepage_url)
          SeedMovieLog.info("Saved Record for Cast Member #{art_name}") if artist.persisted?
          artist_indexes << artist.id
        end
      end
    end
    artist_indexes
    Moviecast.link_movie_and_artists(artist_indexes, artist_char_data_pl, movie)
  end

  private

  def set_profile_url
    unless self.art_profile_photo.nil?
      self.art_profile_photo = ARTIST_BASE_URL + art_profile_photo
    end
  end

end
