# == Schema Information
#
# Table name: artists
#
#  id                :bigint(8)        not null, primary key
#  art_name          :string
#  art_tmdb_id       :integer
#  art_dob           :date
#  art_dod           :date
#  art_primary_skill :string
#  art_aliases       :string           default([]), is an Array
#  art_gender        :string
#  art_biography     :text
#  art_popularity    :float
#  art_birth_place   :string
#  art_profile_photo :string
#  art_imdb_id       :string
#  art_homepage_url  :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

require 'test_helper'

class ArtistTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :art_name
  should have_db_column :art_tmdb_id
  should have_db_column :art_dob
  should have_db_column :art_dod
  should have_db_column :art_primary_skill
  should have_db_column :art_aliases
  should have_db_column :art_gender
  should have_db_column :art_biography
  should have_db_column :art_popularity
  should have_db_column :art_birth_place
  should have_db_column :art_profile_photo
  should have_db_column :art_imdb_id
  should have_db_column :art_homepage_url
  should have_db_column :created_at
  should have_db_column :updated_at
end
