# == Schema Information
#
# Table name: comments
#
#  id           :bigint(8)        not null, primary key
#  comment_body :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  review_id    :bigint(8)
#

require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :comment_body
  should have_db_column :created_at
  should have_db_column :updated_at
  should belong_to :review
end
