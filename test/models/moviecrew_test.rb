# == Schema Information
#
# Table name: moviecrews
#
#  id         :bigint(8)        not null, primary key
#  movie_id   :integer
#  crew_id    :integer
#  crew_title :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class MoviecrewTest < ActiveSupport::TestCase
  should have_db_column :id
  should belong_to :movie
  should belong_to :crew
  should have_db_column :crew_title
  should have_db_column :created_at
  should have_db_column :updated_at
end
