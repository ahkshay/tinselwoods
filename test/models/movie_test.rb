# == Schema Information
#
# Table name: movies
#
#  id                    :bigint(8)        not null, primary key
#  mov_title             :string
#  mov_plot              :text
#  mov_run_time          :integer
#  mov_release_date      :date
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  mov_release_year      :integer
#  mov_country           :string
#  mov_poster_url        :string
#  mov_imdb_reference_id :string
#  mov_imdb_rating       :float
#  mov_youtube_url       :string
#  mov_genres            :text             default([]), is an Array
#  mov_backdrop_url      :string
#  mov_tmdb_id           :integer
#  mov_language          :string
#  mov_status            :string
#  mov_tagline           :string
#  mov_budget            :integer
#  mov_revenue           :integer
#

require 'test_helper'

class MovieTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :mov_title
  should have_db_column :mov_plot
  should have_db_column :mov_run_time
  should have_db_column :mov_release_date
  should have_db_column :created_at
  should have_db_column :updated_at
  should have_db_column :mov_release_year
  should have_db_column :mov_country
  should have_db_column :mov_poster_url
  should have_db_column :mov_imdb_reference_id
  should have_db_column :mov_imdb_rating
  should have_db_column :mov_youtube_url
  should have_db_column :mov_genres
  should have_db_column :mov_tagline
  should have_db_column :mov_backdrop_url
  should have_db_column :mov_tmdb_id
  should have_db_column :mov_language
  should have_db_column :mov_status
  should have_db_column :mov_budget
  should have_db_column :mov_revenue
end
