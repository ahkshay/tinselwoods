# == Schema Information
#
# Table name: production_houses
#
#  id              :bigint(8)        not null, primary key
#  ph_name         :string
#  ph_country      :string
#  ph_headquarters :string
#  ph_description  :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  ph_homepage_url :string
#  ph_logo_path    :string
#  ph_tmdb_id      :integer
#

require 'test_helper'

class ProductionHouseTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :ph_name
  should have_db_column :ph_country
  should have_db_column :ph_headquarters
  should have_db_column :ph_description
  should have_db_column :created_at
  should have_db_column :updated_at
  should have_db_column :ph_homepage_url
  should have_db_column :ph_logo_path
  should have_db_column :ph_tmdb_id
end
