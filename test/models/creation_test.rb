# == Schema Information
#
# Table name: creations
#
#  id                  :bigint(8)        not null, primary key
#  movie_id            :bigint(8)
#  production_house_id :bigint(8)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'test_helper'

class CreationTest < ActiveSupport::TestCase
  should have_db_column :id
  should belong_to :movie
  should belong_to :production_house
  should have_db_column :created_at
  should have_db_column :updated_at
end
