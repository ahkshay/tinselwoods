# == Schema Information
#
# Table name: movie_posters
#
#  id               :bigint(8)        not null, primary key
#  movie_id         :bigint(8)
#  mov_backdrop_url :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require "test_helper"

class MovieBackdropTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :mov_backdrop_url
  should have_db_column :created_at
  should have_db_column :updated_at
  should belong_to :movie
end
