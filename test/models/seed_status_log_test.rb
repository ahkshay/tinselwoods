# == Schema Information
#
# Table name: seed_status_logs
#
#  id                  :bigint(8)        not null, primary key
#  seed_parameters     :string
#  seed_entity_type    :string
#  seed_request_status :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'test_helper'

class SeedStatusLogTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :seed_parameters
  should have_db_column :seed_entity_type
  should have_db_column :seed_request_status
  should have_db_column :created_at
  should have_db_column :updated_at
end
