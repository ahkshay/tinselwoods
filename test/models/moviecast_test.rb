# == Schema Information
#
# Table name: moviecasts
#
#  id             :bigint(8)        not null, primary key
#  artist_id      :integer
#  movie_id       :integer
#  character_name :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'test_helper'

class MoviecastTest < ActiveSupport::TestCase
  should have_db_column :id
  should belong_to :artist
  should belong_to :movie
  should have_db_column :character_name
  should have_db_column :created_at
  should have_db_column :updated_at
end
