# == Schema Information
#
# Table name: reviews
#
#  id                    :bigint(8)        not null, primary key
#  review_title          :string
#  review_body           :text
#  review_watchable      :string
#  is_review_publishable :boolean
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

require 'test_helper'

class ReviewTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :review_title
  should have_db_column :review_body
  should have_db_column :review_watchable
  should have_db_column :is_review_publishable
  should have_db_column :created_at
  should have_db_column :updated_at
end
