# == Schema Information
#
# Table name: crews
#
#  id                     :bigint(8)        not null, primary key
#  crw_dob                :date
#  crw_dod                :date
#  crw_primary_department :string
#  crw_tmdb_id            :integer
#  crw_name               :string
#  crw_aliases            :string           default([]), is an Array
#  crw_biography          :text
#  crw_popularity         :float
#  crw_birth_place        :string
#  crw_profile_photo      :string
#  crw_homepage_url       :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  crw_imdb_id            :integer
#  crw_gender             :string
#

require 'test_helper'

class CrewTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :crw_dob
  should have_db_column :crw_dod
  should have_db_column :crw_primary_department
  should have_db_column :crw_tmdb_id
  should have_db_column :crw_name
  should have_db_column :crw_aliases
  should have_db_column :crw_biography
  should have_db_column :crw_popularity
  should have_db_column :crw_birth_place
  should have_db_column :crw_profile_photo
  should have_db_column :crw_homepage_url
  should have_db_column :created_at
  should have_db_column :updated_at
  should have_db_column :crw_imdb_id
  should have_db_column :crw_gender
end
