# == Schema Information
#
# Table name: languages
#
#  id            :bigint(8)        not null, primary key
#  lang_name     :string
#  lang_iso_code :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'test_helper'

class LanguageTest < ActiveSupport::TestCase
  should have_db_column :id
  should have_db_column :lang_name
  should have_db_column :lang_iso_code
  should have_db_column :created_at
  should have_db_column :updated_at
end
