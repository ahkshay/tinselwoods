class MovieControllerTest < ActionDispatch::IntegrationTest

 test "Placing An Valid Request For Seeding Movie Data" do
  # Test Setup Goes Here.....
  get seed_new_movie_request_url, params: { mov_title: 'Bunty Aur Babli', mov_language: 'Hindi' }
  p "Request Params are #{@request.query_parameters}" if Rails.env.test?         #For Debugging Purpose.....
  # Assertions Check Goes Here.....
  assert_response :success
  assert_routing({ method: 'get', path: '/movie/new_movie_seed_request' }, { controller: "movie", action: "new_movie_seed_request" })
  assert_equal 'Bunty Aur Babli', @request.query_parameters['mov_title'], 'Title Match Successful'
  assert_equal 'Hindi', @request.query_parameters['mov_language'], 'Language Match Successful'
end

# test "Upon Placing An Valid Request For Movie Seeding" do
#   # Test Setup Goes Here.....
#   post seed_movie_data_api_url, params: { mov_title: 'Rang De Basanti', mov_language: 'Hindi' }
#   p "Request Params are #{@request.request_parameters}" if Rails.env.test?       #For Debugging Purpose.....
#   # Assertions Check Goes Here.....
#   assert_response :success
#   assert_routing({ method: 'post', path: '/movie/seed_request_for_movie' }, { controller: "movie", action: "seed_request_for_movie" })
#   assert_equal 'Rang De Basanti', @request.request_parameters['mov_title'], 'Title Match Successful'
#   assert_equal 'Hindi', @request.request_parameters['mov_language'], 'Language Match Successful'
#   assert_equal 'Request for seeding has been submitted successfully', flash[:success]
# end

test "Upon Placing An Invalid Request For Movie Seeding" do
  # Test Setup Goes Here.....
  post seed_movie_data_api_url, params: { mov_title: 'Kodi', mov_language: '' }
  p "Request Params are #{@request.request_parameters}" if Rails.env.test?       #For Debugging Purpose.....
  # Assertions Check Goes Here.....
  assert_response :redirect
  assert_routing({ method: 'post', path: '/movie/seed_request_for_movie' }, { controller: "movie", action: "seed_request_for_movie" })
  assert_equal 'Kodi', @request.request_parameters['mov_title'], 'Title Match Successful'
  assert_equal '', @request.request_parameters['mov_language'], 'Language Match Successful'
  assert_redirected_to controller: "movie", action: "new_movie_seed_request"
  assert_equal 'Please enter valid movie title and movie language in order to proceed with the seeding request', flash[:danger]
end

end
