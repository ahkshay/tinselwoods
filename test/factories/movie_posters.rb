# == Schema Information
#
# Table name: movie_posters
#
#  id               :bigint(8)        not null, primary key
#  movie_id         :bigint(8)
#  mov_backdrop_url :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

FactoryBot.define do
  factory :movie_poster do
    movie { nil }
    mov_poster_url { "MyString" }
  end
end
