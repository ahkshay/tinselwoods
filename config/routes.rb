Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => 'production_house#index', :as => :dashboard
  get 'production_house/:id/show' => 'production_house#show', :as => :production_house_show
  get 'production_house/:id/edit' => 'production_house#edit', :as => :production_house_edit
  patch 'production_house/:id/update' => 'production_house#update', :as => :production_house_update
  get 'movie/new_movie_seed_request' => 'movie#new_movie_seed_request', :as => :seed_new_movie_request
  post 'movie/seed_request_for_movie' => 'movie#seed_request_for_movie', :as => :seed_movie_data_api

end
