require 'csv'
namespace :seed_prod_house_data do

  desc "Seed Production House Data"
  task :create_production_houses => :environment do |t, args|
    CSV.foreach(Rails.root.join('data', 'production_houses_master_list.csv'), headers: true) do |row|
      ProductionHouse.create(:ph_name => row['Name'], :ph_country => row['Country'], :ph_headquarters => row['Headquarters'], :ph_description => row['Description'], :ph_establishment_year => row['Establishment_Year'])
    end
  end

end
