class AddLanguageToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :mov_language, :string
  end
end
