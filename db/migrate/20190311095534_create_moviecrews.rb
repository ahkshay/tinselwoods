class CreateMoviecrews < ActiveRecord::Migration[5.2]
  def change
    create_table :moviecrews do |t|
      t.integer :movie_id
      t.integer :crew_id
      t.string :crew_title

      t.timestamps
    end
    add_index :moviecrews, [:movie_id, :crew_id]
  end
end
