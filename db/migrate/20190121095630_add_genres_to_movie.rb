class AddGenresToMovie < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :mov_genres, :text, array: true, default: []
  end
end
