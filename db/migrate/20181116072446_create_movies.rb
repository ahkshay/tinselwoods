class CreateMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :movies do |t|
      t.string :mov_title
      t.text :mov_description
      t.integer :mov_running_time
      t.date :mov_release_date

      t.timestamps
    end
  end
end
