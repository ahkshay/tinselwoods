class DropDirectorWorks < ActiveRecord::Migration[5.2]

  def up
    drop_table :director_works
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end

end
