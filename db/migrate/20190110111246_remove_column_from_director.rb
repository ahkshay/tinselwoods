class RemoveColumnFromDirector < ActiveRecord::Migration[5.2]
  def change
    remove_column :directors, :dir_first_name, :string
    remove_column :directors, :dir_last_name, :string
  end
end
