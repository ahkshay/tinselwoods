class AddProductionHouseToMovies < ActiveRecord::Migration[5.2]
  def change
    add_reference :movies, :production_house, foreign_key: true
  end
end
