class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.string :review_title
      t.text :review_body
      t.string :review_watchable
      t.boolean :is_review_publishable

      t.timestamps
    end
  end
end
