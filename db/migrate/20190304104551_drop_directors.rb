class DropDirectors < ActiveRecord::Migration[5.2]

  def up
    drop_table :directors
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end

end
