class SchemaRevisitForMovie < ActiveRecord::Migration[5.2]
  def change
    rename_column :movies, :mov_description, :mov_plot
    change_column :movies, :mov_language, "varchar[] USING (string_to_array(mov_language, ','))"
    add_column :movies, :mov_country, :string
    add_column :movies, :mov_poster_url, :string
    add_column :movies, :mov_imdb_reference_id, :string
    add_column :movies, :mov_imdb_rating, :float
  end
end
