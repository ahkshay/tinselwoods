class RemoveMovieLanguageFromMovie < ActiveRecord::Migration[5.2]
  def change
    remove_column :movies, :mov_language, :string
  end
end
