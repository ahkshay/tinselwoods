class CreateMoviecasts < ActiveRecord::Migration[5.2]
  def change
    create_table :moviecasts do |t|
      t.integer :artist_id
      t.integer :movie_id
      t.string :character_name

      t.timestamps
    end
    add_index :moviecasts, [:movie_id, :artist_id]
  end
end
