class FixColumnNameInMovie < ActiveRecord::Migration[5.2]
  def change
    rename_column :movies, :mov_running_time, :mov_run_time
  end
end
