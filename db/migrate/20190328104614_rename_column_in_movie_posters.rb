class RenameColumnInMoviePosters < ActiveRecord::Migration[5.2]
  def change
    rename_column :movie_posters, :mov_poster_url, :mov_backdrop_url
  end
end
