class CreateProductionHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :production_houses do |t|
      t.string :ph_name
      t.string :ph_country
      t.string :ph_headquarters
      t.integer :ph_establishment_year
      t.text :ph_description

      t.timestamps
    end
  end
end
