class AddCrewGenderToCrews < ActiveRecord::Migration[5.2]
  def change
    add_column :crews, :crw_gender, :string
  end
end
