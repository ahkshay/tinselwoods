class RemoveForeignKeyMovies < ActiveRecord::Migration[5.2]
  def change
    remove_reference(:movies, :genre, index: true)
  end
end
