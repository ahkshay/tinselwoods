class AddColumnsToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :mov_status, :string
    add_column :movies, :mov_tagline, :string
  end
end
