class AddColumnToDirector < ActiveRecord::Migration[5.2]
  def change
    add_column :directors, :dir_full_name, :string
  end
end
