class RenameColumnInMovies < ActiveRecord::Migration[5.2]
  def change
    rename_column :movies, :mov_youtube_id, :mov_youtube_url
  end
end
