class RemoveColumnFromReview < ActiveRecord::Migration[5.2]
  def change
    remove_column :reviews, :comment_id, :integer
  end
end
