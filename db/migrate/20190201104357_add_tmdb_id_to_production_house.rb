class AddTmdbIdToProductionHouse < ActiveRecord::Migration[5.2]
  def change
    add_column :production_houses, :ph_tmdb_id, :integer
  end
end
