class CreateMoviePosters < ActiveRecord::Migration[5.2]
  def change
    create_table :movie_posters do |t|
      t.references :movie, foreign_key: true
      t.string :mov_poster_url

      t.timestamps
    end
  end
end
