class DropGenresTable < ActiveRecord::Migration[5.2]

  def up
    drop_table :genres
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
