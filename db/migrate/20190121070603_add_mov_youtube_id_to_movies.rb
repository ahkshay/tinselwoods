class AddMovYoutubeIdToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :mov_youtube_id, :string
  end
end
