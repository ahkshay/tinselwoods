class RenameMoviePostersToMovieBackdrops < ActiveRecord::Migration[5.2]
  def change
    rename_table :movie_posters, :movie_backdrops
  end
end
