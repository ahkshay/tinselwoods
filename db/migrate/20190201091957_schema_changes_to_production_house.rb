class SchemaChangesToProductionHouse < ActiveRecord::Migration[5.2]
  def change
    remove_column :production_houses, :ph_establishment_year
    add_column :production_houses, :ph_homepage_url, :string
    add_column :production_houses, :ph_logo_path, :string
  end
end
