class AddMovieFinancialDataToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :mov_budget, :integer
    add_column :movies, :mov_revenue, :integer
  end
end
