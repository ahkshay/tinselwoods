class CreateArtists < ActiveRecord::Migration[5.2]
  def change
    create_table :artists do |t|
      t.string :art_name
      t.integer :art_tmdb_id
      t.date :art_dob
      t.date :art_dod
      t.string :art_primary_skill
      t.string :art_aliases, array: true, default: []
      t.string :art_gender
      t.text :art_biography
      t.float :art_popularity
      t.string :art_birth_place
      t.string :art_profile_photo
      t.string :art_imdb_id
      t.string :art_homepage_url

      t.timestamps
    end
  end
end
