class RemoveProductionHouseFromMovies < ActiveRecord::Migration[5.2]
  def change
    remove_reference :movies, :production_house, foreign_key: true
  end
end
