class CreateSeedStatusLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :seed_status_logs do |t|
      t.string :seed_parameters
      t.string :seed_entity_type
      t.boolean :seed_request_status

      t.timestamps
    end
  end
end
