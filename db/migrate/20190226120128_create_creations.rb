class CreateCreations < ActiveRecord::Migration[5.2]
  def change
    create_table :creations do |t|
      t.belongs_to :movie, index: true
      t.belongs_to :production_house, index:true
      t.timestamps
    end
  end
end
