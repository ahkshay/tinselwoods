class AddPosterPathToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :mov_poster_path, :string
  end
end
