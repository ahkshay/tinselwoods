class CreateCrews < ActiveRecord::Migration[5.2]
  def change
    create_table :crews do |t|
      t.date :crw_dob
      t.date :crw_dod
      t.string :crw_primary_department
      t.integer :crw_tmdb_id
      t.string :crw_name
      t.string :crw_aliases, array: true, default: []
      t.text :crw_biography
      t.float :crw_popularity
      t.string :crw_birth_place
      t.string :crw_profile_photo
      t.integer :crw_tmdb_id
      t.string :crw_homepage_url

      t.timestamps
    end
  end
end
