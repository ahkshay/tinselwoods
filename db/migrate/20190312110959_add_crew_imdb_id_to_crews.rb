class AddCrewImdbIdToCrews < ActiveRecord::Migration[5.2]
  def change
    add_column :crews, :crw_imdb_id, :integer
  end
end
