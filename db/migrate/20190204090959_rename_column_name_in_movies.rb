class RenameColumnNameInMovies < ActiveRecord::Migration[5.2]
  def change
    rename_column :movies, :mov_poster_path, :mov_backdrop_url
  end
end
