class AddColumnToMovies < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :mov_release_year, :integer
  end
end
