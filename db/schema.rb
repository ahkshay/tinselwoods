# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_28_105330) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "artists", force: :cascade do |t|
    t.string "art_name"
    t.integer "art_tmdb_id"
    t.date "art_dob"
    t.date "art_dod"
    t.string "art_primary_skill"
    t.string "art_aliases", default: [], array: true
    t.string "art_gender"
    t.text "art_biography"
    t.float "art_popularity"
    t.string "art_birth_place"
    t.string "art_profile_photo"
    t.string "art_imdb_id"
    t.string "art_homepage_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text "comment_body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "review_id"
    t.index ["review_id"], name: "index_comments_on_review_id"
  end

  create_table "creations", force: :cascade do |t|
    t.bigint "movie_id"
    t.bigint "production_house_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["movie_id"], name: "index_creations_on_movie_id"
    t.index ["production_house_id"], name: "index_creations_on_production_house_id"
  end

  create_table "crews", force: :cascade do |t|
    t.date "crw_dob"
    t.date "crw_dod"
    t.string "crw_primary_department"
    t.integer "crw_tmdb_id"
    t.string "crw_name"
    t.string "crw_aliases", default: [], array: true
    t.text "crw_biography"
    t.float "crw_popularity"
    t.string "crw_birth_place"
    t.string "crw_profile_photo"
    t.string "crw_homepage_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "crw_imdb_id"
    t.string "crw_gender"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "languages", force: :cascade do |t|
    t.string "lang_name"
    t.string "lang_iso_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "movie_backdrops", force: :cascade do |t|
    t.bigint "movie_id"
    t.string "mov_backdrop_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["movie_id"], name: "index_movie_backdrops_on_movie_id"
  end

  create_table "moviecasts", force: :cascade do |t|
    t.integer "artist_id"
    t.integer "movie_id"
    t.string "character_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "moviecrews", force: :cascade do |t|
    t.integer "movie_id"
    t.integer "crew_id"
    t.string "crew_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "movies", force: :cascade do |t|
    t.string "mov_title"
    t.text "mov_plot"
    t.integer "mov_run_time"
    t.date "mov_release_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "mov_release_year"
    t.string "mov_country"
    t.string "mov_poster_url"
    t.string "mov_imdb_reference_id"
    t.float "mov_imdb_rating"
    t.string "mov_youtube_url"
    t.text "mov_genres", default: [], array: true
    t.string "mov_backdrop_url"
    t.integer "mov_tmdb_id"
    t.string "mov_language"
    t.string "mov_status"
    t.string "mov_tagline"
    t.integer "mov_budget"
    t.integer "mov_revenue"
  end

  create_table "production_houses", force: :cascade do |t|
    t.string "ph_name"
    t.string "ph_country"
    t.string "ph_headquarters"
    t.text "ph_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ph_homepage_url"
    t.string "ph_logo_path"
    t.integer "ph_tmdb_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.string "review_title"
    t.text "review_body"
    t.string "review_watchable"
    t.boolean "is_review_publishable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seed_status_logs", force: :cascade do |t|
    t.string "seed_parameters"
    t.string "seed_entity_type"
    t.boolean "seed_request_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "comments", "reviews"
  add_foreign_key "movie_backdrops", "movies"
end
